/**
 * 
 */
package org.bitbucket.fredgrott.gwseulahelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;


/**
 * A helper for showing EULAs and storing a {@link SharedPreferences} bit 
 * indicating whether the user has accepted. Call as EULAHelper() 
 * and you can override the string resoruce settings in you own 
 * application res/values/strings.xml file.
 * 
 * From iosched app under Apache 2.0 license
 * 
 */
public final class EULAHelper {
    
    /**
     * Instantiates a new eula helper.
     */
    private EULAHelper() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Checks for accepted eula.
     *
     * @param context the context
     * @return true, if successful
     */
    public static boolean hasAcceptedEula(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean("accepted_eula", false);
    }

    /**
     * Sets the accepted eula.
     *
     * @param context the new accepted eula
     */
    private static void setAcceptedEula(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... voids) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                sp.edit().putBoolean("accepted_eula", true).commit();
                return null;
            }
        } .execute();
    }

    /**
     * Show End User License Agreement.
     *
     * @param accepted True IFF user has accepted license already, which means it can be dismissed.
     *                 If the user hasn't accepted, then the EULA must be accepted or the program
     *                 exits.
     * @param activity Activity started from.
     */
    public static void showEula(final boolean accepted, final Activity activity) {
        AlertDialog.Builder eula = new AlertDialog.Builder(activity)
                .setTitle(R.string.eula_title)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(R.string.eula_text)
                .setCancelable(accepted);

        if (accepted) {
            // If they've accepted the EULA allow, show an OK to dismiss.
            eula.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            // If they haven't accepted the EULA allow, show accept/decline buttons and exit on
            // decline.
            eula
                    .setPositiveButton(R.string.accept,
                            new android.content.DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    setAcceptedEula(activity);
                                    dialog.dismiss();
                                }
                            })
                    .setNegativeButton(R.string.decline,
                            new android.content.DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    activity.finish();
                                }
                            });
        }
        eula.show();
    }
}