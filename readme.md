![EULA image](https://lh5.googleusercontent.com/-OhtXoEqWRZI/UMihU0JpfcI/AAAAAAAAA4c/mnbRDbZfwYg/s376/pbupdator_eula.gif)

GWSEULAHelper
---

EULAHelper Android Project Library for android application development.
As you may now because of the recent Etsy court case and new CA laws you 
need or should have an EULA that not only states the terms if you connect or 
offer a service but alos details the privacy policy for any 
end user information that you might use AND IT SHOULD BE enforced 
in such a way that user cannot use the application until agreeing to the 
EULA. Hence this androdi project library.



# Project License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

# Project Usage

Just set it up in your IDE workspace as a normal android project library.
Than callit like this:



# Project Credits

Original code was part of Google IO App and is under Apache License 2.0.
Modifications by Fred Grott.

## Fred Grott

[Fred Grott's blog](http://grottworkshop.blogspot.com)

[Fred Grott's website](http://fredgrott.bitbucket.org)
